package com.phararts.rfid_reader.daemon;

import com.phidgets.PhidgetException;
import com.phidgets.RFIDPhidget;
import com.phidgets.event.AttachEvent;
import com.phidgets.event.AttachListener;
import com.phidgets.event.DetachEvent;
import com.phidgets.event.DetachListener;
import com.phidgets.event.ErrorEvent;
import com.phidgets.event.ErrorListener;
import com.phidgets.event.TagGainEvent;
import com.phidgets.event.TagGainListener;
import com.phidgets.event.TagLossEvent;
import com.phidgets.event.TagLossListener;
import java.awt.AWTException;
import org.apache.log4j.Logger;
import com.phararts.rfid_reader.robot.SmartRobot;

/**
 * This daemon outputs connects to an avalaible phidgets RFID device to outputs read codes in current cursor 
 * potition like a keyboard.
 * @author Aday Talavera <aday.talavera at gmail.com>
 */
public class RFIDDaemon
{
  private static final Logger logger = Logger.getLogger(RFIDDaemon.class);

  private RFIDPhidget rfid;
  private RFIDAttachListener attachListener;
  private RFIDDetachListener dettachListener;
  private ErrorListener errorListener;
  private TagGainListener tagGainListener;
  private TagLossListener tagLossListener;  

  private final Object mutex = new Object();
  
  private RFIDDaemon()
  {
    logger.info("Creating RFID daemon");
    
    // When the VM shutdowns, this thread disposes Phidget device silently
    Runtime.getRuntime().addShutdownHook(new Thread(new Runnable()
    {
      public void run()
      {
        try
        {
          dispose();
        }
        catch(RFIDException ignore)
        {
        }
      }
    }
    ));
  }  

  /**
   * Initializes the daemon to wait for a RFID Phidget connection
   * @throws RFIDException
   */
  private void init() throws RFIDException
  {
    boolean error = true;
    logger.info("Initializing RFID daemon");
    try
    {      
      rfid = new RFIDPhidget();

      attachListener = new RFIDAttachListener();
      dettachListener = new RFIDDetachListener();
      errorListener = new RFIDErrorListener();
      tagGainListener = new RFIDTagGainListener(new SmartRobot(10));
      tagLossListener = new RFIDTagLossListener();

      rfid.addAttachListener(attachListener);
      rfid.addDetachListener(dettachListener);
      rfid.addErrorListener(errorListener);
      rfid.addTagGainListener(tagGainListener);
      rfid.addTagLossListener(tagLossListener);
                
      rfid.openAny();
      
      error = false;
      logger.info("RFID daemon initialized");
    }
    catch (PhidgetException ex)
    {            
      throw new RFIDException("Error while initializing RFIDPhidget", ex);
    }
    catch (AWTException ex)
    {      
      throw new RFIDException("Unable to create keyboard robot", ex);
    }
    catch(Throwable t)
    {
      logger.error("Unexpected error while initializing phidgets RFID device", t);
    }
    finally
    {
      if(error) System.exit(1);      
    }
  }

  /**
   * Disposes the daemon and the device resources
   * @throws RFIDException
   */
  private void dispose() throws RFIDException
  {    
    try
    {
      if (rfid != null)
      {
        logger.info("Disposing RFID daemon");
        rfid.removeAttachListener(attachListener);
        rfid.removeDetachListener(dettachListener);
        rfid.removeErrorListener(errorListener);
        rfid.removeTagGainListener(tagGainListener);
        rfid.removeTagLossListener(tagLossListener);

        attachListener = null;
        dettachListener = null;
        errorListener = null;
        tagGainListener = null;
        tagLossListener = null;

        rfid.close();
        rfid = null;
        logger.info("RFID daemon disposed");
      }
    }
    catch (PhidgetException ex)
    {      
      throw new RFIDException("Error while disposing RFIDPhidget", ex);
    }
  }

  /**
   * Starts the daemon to wait for a RFID phidget device to be attached
   */
  public void start()
  {
    (new Thread(new StartDaemonThread())).start();    
  }

  /**
   * Return true if a RFID device is connected
   * @return
   */
  public boolean isConnected()
  {
    try
    {
      return rfid != null && rfid.isAttached();
    }
    catch(PhidgetException ignore)
    {
    }
    return false;
  }

  /**
   * Return the RFID device serial number or -1 if a device is not connected
   * @return
   */
  public int getSerialNumber()
  {    
    if(isConnected())
    {
      try
      {
        return rfid.getSerialNumber();
      }
      catch(PhidgetException ignore)
      {
      }
    }    
    return -1;
  }

  /**
   * Return the last tag readed from the RFID device or an empty String if a device is not connected
   * @return
   */
  public String getLastReadedTag()
  {
    if(isConnected())
    {
      try
      {
        return rfid.getLastTag();
      }
      catch(PhidgetException ignore)
      {
      }
    }
    return "";
  }

  private static RFIDDaemon instance;

  /**
   * Get RFIDDaemon single instance
   * @return
   */
  public static RFIDDaemon getInstance()
  {
    if(instance == null)
    {
      instance = new RFIDDaemon();
    }
    return instance;
  }

  ///////////////////////////////////
  // Phidget Listeners
  ///////////////////////////////////
  private class StartDaemonThread implements Runnable
  {
    public void run()
    {      
      try
      {        
        // Initializes rfid device
        init();                
      }
      catch (RFIDException ex)
      {
        logger.error("Error while initializing RFID", ex);
        return;
      }
      
      // Try to reconnect to a rfid device infinitly
      while(true)
      {
        // Reconnect to a phidget device
        if(!isConnected())
        {
          logger.info("Waiting for RFIDPhidget attachment");
          try
          {          
            rfid.waitForAttachment();
          }
          catch (PhidgetException ex)
          {
            ex.printStackTrace();
          }
        }
        try
        {
          synchronized(mutex)
          {
            mutex.wait();                        
          }
        }          
        catch (InterruptedException ignore)
        {            
        }        
      }      
    }
  }

  ///////////////////////////////////
  // Phidget Listeners
  ///////////////////////////////////
  private class RFIDAttachListener implements AttachListener
  {
    public void attached(AttachEvent ae)
    {
      try
      {
        RFIDPhidget device = (RFIDPhidget) ae.getSource();
        logger.info("RFIDPhidget attached, serial number " + device.getSerialNumber());
        device.setAntennaOn(true);
      }
      catch (PhidgetException ex)
      {
        logger.error("Error while turning On antenna", ex);
      }
    }
  }

  private class RFIDDetachListener implements DetachListener
  {
    public void detached(DetachEvent de)
    {      
      logger.info("RFIDPhidget detached");
      synchronized(mutex)
      {
        mutex.notifyAll();
      }
    }
  }

  private class RFIDErrorListener implements ErrorListener
  {
    public void error(ErrorEvent ee)
    {
      logger.error("Error with RFIDPhidget", ee.getException());
    }
  }

  private class RFIDTagGainListener implements TagGainListener
  {
    private SmartRobot robot;

    public RFIDTagGainListener(SmartRobot robot)
    {
      this.robot = robot;
    }

    public void tagGained(TagGainEvent tge)
    {
      String code = tge.getValue();
      
      if(logger.isDebugEnabled()) logger.debug("Tag gained: " + code);
      
      robot.type(code);
      robot.keyType(java.awt.event.KeyEvent.VK_ENTER);
      robot.waitForIdle();

      // Turn on LED
      try
      {
        ((RFIDPhidget) tge.getSource()).setLEDOn(true);
      }
      catch (PhidgetException ex)
      {
        logger.error("Error while turning On LED", ex);
      }
    }
  }

  private class RFIDTagLossListener implements TagLossListener
  {
    public void tagLost(TagLossEvent tle)
    {
      if(logger.isDebugEnabled()) logger.debug("Tag lost: " + tle.getValue());
      
      // Turn off LED
      try
      {
        ((RFIDPhidget) tle.getSource()).setLEDOn(false);
      }
      catch (PhidgetException ex)
      {
        logger.error("Error while turning Off LED", ex);
      }
    }
  }
}
