package com.phararts.rfid_reader.daemon;

/**
 *
 * @author Aday Talavera <aday.talavera at gmail.com>
 */
public class RFIDException extends Exception
{

  public RFIDException(Throwable thrwbl)
  {
    super(thrwbl);
  }

  public RFIDException(String string, Throwable thrwbl)
  {
    super(string, thrwbl);
  }

  public RFIDException(String string)
  {
    super(string);
  }

  public RFIDException()
  {
  }
}
