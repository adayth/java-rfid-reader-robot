package com.phararts.rfid_reader.ui;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionListener;
import org.apache.log4j.Logger;

/**
 *
 * @author Felfo
 */
public class SystemTrayIcon
{
  private static final Logger logger = Logger.getLogger(SystemTrayIcon.class);

  private TrayIcon trayIcon;

  public SystemTrayIcon(Image icon, String tooltip, PopupMenu menu)
  {    
    trayIcon = new TrayIcon(icon, tooltip, menu);
    trayIcon.setImageAutoSize(true);    
  }  
  
  public void setTooltip(String text)
  {
    trayIcon.setToolTip(text);
  }
  
  public void addTrayIconToSystem()
  {
    try
    {
      SystemTray.getSystemTray().add(trayIcon);
    }
    catch (AWTException ex)
    {
      logger.error("TrayIcon could not be added", ex);
    }
  }
  
  public void removeTrayIconFromSystem()
  {
    SystemTray.getSystemTray().remove(trayIcon);
  }
  
  public static boolean isSupported()
  {
    return SystemTray.isSupported();
  }
  
  public void addActionListener(ActionListener listener)
  {
    trayIcon.addActionListener(listener);
  }
}
