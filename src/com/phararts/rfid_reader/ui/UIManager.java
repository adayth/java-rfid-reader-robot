package com.phararts.rfid_reader.ui;

import com.phararts.rfid_reader.ui.icons.IconManager;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.apache.log4j.Logger;

/**
 *
 * @author Felfo
 */
public class UIManager
{
  private static final Logger logger = Logger.getLogger(UIManager.class);

  private SystemTrayIcon trayIcon;
  private RFIDStatusJFrame statusJFrame;

  private boolean createdUI = false;

  private UIManager()
  {    
  }
  
  // create UI
  public void createUI()
  {
    if(!createdUI)
    {
      if(!SystemTrayIcon.isSupported())
      {
        logger.error("Trayicon is not supported in this system");
        return;
      }

      statusJFrame = new RFIDStatusJFrame();

      Image image = IconManager.getImageFromIcon(IconManager.READER_ICON);

      PopupMenu menu = new PopupMenu();

      MenuItem logItem = new MenuItem("Show status");
      logItem.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            statusJFrame.showCentered();
          }
        }
      );
      menu.add(logItem);

      menu.addSeparator();

      MenuItem exitItem = new MenuItem("Exit");
      exitItem.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            disposeUI();
            System.exit(0);
          }
        }
      );
      menu.add(exitItem);
      
      ActionListener listener = new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          statusJFrame.showCentered();
        }        
      };

      trayIcon = new SystemTrayIcon(image, "RFID Reader", menu);
      trayIcon.addTrayIconToSystem();
      trayIcon.addActionListener(listener);
      createdUI = true;
    }
  }
  
  // dispoe UI
  public void disposeUI()
  {
    if(createdUI)
    {
      statusJFrame.setVisible(false);
      statusJFrame.dispose();

      trayIcon.removeTrayIconFromSystem();
      
      createdUI = false;
    }
  }
  
  // Singleton
  private static UIManager instance;
    
  public static UIManager getInstance()
  {
    if(instance == null)
    {
      instance = new UIManager();
    }
    return instance;
  }
}
