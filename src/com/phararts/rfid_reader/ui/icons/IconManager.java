package com.phararts.rfid_reader.ui.icons;

import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;
import org.apache.log4j.Logger;

public class IconManager
{
  private static final Logger logger = Logger.getLogger(IconManager.class);
  
  private static final String ICON_PATH = "/com/phararts/rfid_reader/ui/icons/";
  
  public static final String READER_ICON = "reader.png";
  public static final String READER_CARD_ICON = "reader_card.png";

  public static Image getImageFromIcon(String iconName)
  {    
    URL url = IconManager.class.getResource(ICON_PATH + iconName);
    if(url == null)
    {
      logger.error("Could not load icon from path: " + ICON_PATH + iconName);
    }
    Image image = Toolkit.getDefaultToolkit().getImage(url);
    return image;
  }
}
