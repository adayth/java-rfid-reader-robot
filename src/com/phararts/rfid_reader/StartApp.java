package com.phararts.rfid_reader;

import com.phararts.rfid_reader.daemon.RFIDDaemon;
import com.phararts.rfid_reader.ui.UIManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * Class to start daemon and configure logging
 * @author Aday Talavera <aday.talavera at gmail.com>
 */
public class StartApp
{
  private static final Logger logger = Logger.getLogger(StartApp.class);

  private static final String LOG_CONFIG_FILE = "conf/log4j.xml";   

  /**
   * Runs Application
   * @param args the command line arguments
   */
  public static void main(String[] args)
  {
    try
    {
      // Configure log with xml file
      DOMConfigurator.configure(LOG_CONFIG_FILE);

      logger.info("Starting application");

      // Create daemon and start it
      RFIDDaemon.getInstance().start();
      UIManager.getInstance().createUI();
    }
    catch(Throwable t)
    {
      logger.error("Unexpected error while initializing daemon", t);
    }
  }
}
