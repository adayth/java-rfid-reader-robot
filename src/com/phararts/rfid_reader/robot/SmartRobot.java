package com.phararts.rfid_reader.robot;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

/**
 * Produces keyboard events to write characters in current cursor position.
 * This improves Robot class to allow writing Strings, isntead of chars.
 * Just use method type(String text) to write.
 * IMPORTANT: Uppercase letters don't work.
 * SOURCE: http://gruimed.blogspot.com/2009/09/using-java-robot-to-type-text-strings.html
 */
public class SmartRobot extends Robot
{
  private int delay;
  
  public SmartRobot() throws AWTException
  {
    this(50);
  }
  
  public SmartRobot(int delay) throws AWTException
  {
    super();
    this.delay = delay;
  }

  public void keyType(int keyCode)
  {
    keyPress(keyCode);
    delay(delay);
    keyRelease(keyCode);
  }

  public void keyType(int keyCode, int keyCodeModifier)
  {
    keyPress(keyCodeModifier);
    keyPress(keyCode);
    delay(delay);
    keyRelease(keyCode);
    keyRelease(keyCodeModifier);
  }

  public void type(String text)
  {
    String textUpper = text.toUpperCase();

    for (int i = 0; i < text.length(); ++i)
    {
      typeChar(textUpper.charAt(i));
      waitForIdle();
    }
  }

  private void typeChar(char c)
  {
    boolean shift = true;
    int keyCode;

    switch (c)
    {
      case '~':
        keyCode = (int) '`';
        break;
      case '!':
        keyCode = (int) '1';
        break;
      case '@':
        keyCode = (int) '2';
        break;
      case '#':
        keyCode = (int) '3';
        break;
      case '$':
        keyCode = (int) '4';
        break;
      case '%':
        keyCode = (int) '5';
        break;
      case '^':
        keyCode = (int) '6';
        break;
      case '&':
        keyCode = (int) '7';
        break;
      case '*':
        keyCode = (int) '8';
        break;
      case '(':
        keyCode = (int) '9';
        break;
      case ')':
        keyCode = (int) '0';
        break;
      case ':':
        keyCode = (int) ';';
        break;
      case '_':
        keyCode = (int) '-';
        break;
      case '+':
        keyCode = (int) '=';
        break;
      case '|':
        keyCode = (int) '\\';
        break;
//  case '"':

//   keyCode = (int)'\'';

//   break;

      case '?':
        keyCode = (int) '/';
        break;
      case '{':
        keyCode = (int) '[';
        break;
      case '}':
        keyCode = (int) ']';
        break;
      case '<':
        keyCode = (int) ',';
        break;
      case '>':
        keyCode = (int) '.';
        break;
      default:
        keyCode = (int) c;
        shift = false;
    }
    if (shift)
    {
      keyType(keyCode, KeyEvent.VK_SHIFT);
    }
    else
    {
      keyType(keyCode);
    }
  }

  private int charToKeyCode(char c)
  {
    switch (c)
    {
      case ':':
        return ';';
    }
    return (int) c;
  }
}
