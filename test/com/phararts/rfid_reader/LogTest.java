package com.phararts.rfid_reader;

import java.io.IOException;
import org.apache.log4j.*;

/**
 * Some log configs and patterns
 * @author Aday Talavera <aday.talavera at gmail.com>
 */
public class LogTest
{
  private static final Logger logger = Logger.getLogger(LogTest.class);

  public static void main(String argv[]) throws IOException
  {
    //BasicConfigurator.configure();

    Logger rootLogger = Logger.getRootLogger();
    rootLogger.setLevel(Level.DEBUG);    
    PatternLayout layout = new PatternLayout("[%d{ISO8601}][%-5p][%t] %c %x - %m%n");
    //PatternLayout layout = new PatternLayout("[%-5.5p][%d{HH:mm:ss,SSS}] %C{1}.%M %m%n");
    rootLogger.addAppender(new ConsoleAppender(layout));
    try
    {
      RollingFileAppender rfa = new RollingFileAppender(layout, "logs/logtest.log");
      rfa.setAppend(false);
      rfa.setMaximumFileSize(1000000);
      rfa.setMaxBackupIndex(3);
      rootLogger.addAppender(rfa);
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }

    logger.debug("Hello world.");
    logger.info("What a beatiful day.");    
  }
}
