package com.phararts.rfid_reader;

import com.phararts.rfid_reader.robot.SmartRobot;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

/**
 * Test to compare standard robot and improved version
 * @author Aday Talavera <aday.talavera at gmail.com>
 */
public class Main
{

  static int keyInput[] =
  {
    KeyEvent.VK_J, KeyEvent.VK_A, KeyEvent.VK_V, KeyEvent.VK_A,
    KeyEvent.VK_SPACE,
    KeyEvent.VK_P, KeyEvent.VK_R, KeyEvent.VK_O, KeyEvent.VK_G, KeyEvent.VK_R, KeyEvent.VK_A, KeyEvent.VK_M, KeyEvent.VK_M, KeyEvent.VK_I, KeyEvent.VK_N, KeyEvent.VK_G,
    KeyEvent.VK_SPACE,
    KeyEvent.VK_F, KeyEvent.VK_O, KeyEvent.VK_R, KeyEvent.VK_U, KeyEvent.VK_M, KeyEvent.VK_S,
    KeyEvent.VK_SPACE,
    KeyEvent.VK_PERIOD, KeyEvent.VK_C, KeyEvent.VK_O, KeyEvent.VK_M,
    KeyEvent.VK_ENTER
  };

  public static void robot() throws AWTException
  {
    Robot robot = new Robot();

    // Con esto cada vez que se envía un evento al robot, se hace un delay del tiempo establecido
    //robot.setAutoWaitForIdle(true);
    //robot.setAutoDelay(100);

    for (int i = 0; i < keyInput.length; i++)
    {
      robot.keyPress(keyInput[i]);
      robot.keyRelease(keyInput[i]);
    }

    robot.waitForIdle();
  }

  public static void smartRobot() throws AWTException
  {
    SmartRobot robot = new SmartRobot();

    robot.type("12345678910");
    robot.keyType(KeyEvent.VK_ENTER);

    robot.waitForIdle();
  }

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) throws AWTException, InterruptedException
  {
    System.out.println("Waiting to execute robot");
    Thread.sleep(2000);

    //robot();
    smartRobot();
  }
}
