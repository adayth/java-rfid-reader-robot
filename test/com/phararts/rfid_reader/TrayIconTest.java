package com.phararts.rfid_reader;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Example to use a TrayIcon
 * @author Aday Talavera <aday.talavera at gmail.com>
 */
public class TrayIconTest
{
  /**
   * @param args the command line arguments
   */
  public static void main(String[] args)
  {
    final TrayIcon trayIcon;

    if (SystemTray.isSupported())
    {

      SystemTray tray = SystemTray.getSystemTray();
      Image image = Toolkit.getDefaultToolkit().getImage("tray.gif");

      MouseListener mouseListener = new MouseListener()
      {
        public void mouseClicked(MouseEvent e)
        {
          System.out.println("Tray Icon - Mouse clicked!");
        }

        public void mouseEntered(MouseEvent e)
        {
          System.out.println("Tray Icon - Mouse entered!");
        }

        public void mouseExited(MouseEvent e)
        {
          System.out.println("Tray Icon - Mouse exited!");
        }

        public void mousePressed(MouseEvent e)
        {
          System.out.println("Tray Icon - Mouse pressed!");
        }

        public void mouseReleased(MouseEvent e)
        {
          System.out.println("Tray Icon - Mouse released!");
        }
      };

      ActionListener exitListener = new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          System.out.println("Exiting...");
          System.exit(0);
        }
      };

      PopupMenu popup = new PopupMenu();
      MenuItem defaultItem = new MenuItem("Exit");
      defaultItem.addActionListener(exitListener);
      popup.add(defaultItem);      

      trayIcon = new TrayIcon(image, "Tray Demo", popup);

      ActionListener actionListener = new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          trayIcon.displayMessage("Action Event", "An Action Event Has Been Performed!", TrayIcon.MessageType.INFO);
        }
      };

      trayIcon.setImageAutoSize(true);
      trayIcon.addActionListener(actionListener);
      //trayIcon.addMouseListener(mouseListener);

      try
      {
        tray.add(trayIcon);
      }
      catch (AWTException e)
      {
        System.err.println("TrayIcon could not be added.");
      }

    }
    else
    {
      //  System Tray is not supported
      System.err.println("System Tray is not supported");
    }
  }
}
